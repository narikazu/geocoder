GoogleGeocoderResult = Struct.new(:status, :results, :error_message) do
  def valid?
    status == 'OK' ? true : false
  end

  def location
    { location: { latitude: latitude, longitude: longitude } }
  end

  def message
    { message: "#{status}: #{error_message}" }
  end

  private

  def latitude
    valid? ? results.first.dig('geometry', 'location', 'lat') : ''
  end

  def longitude
    valid? ? results.first.dig('geometry', 'location', 'lng') : ''
  end
end
