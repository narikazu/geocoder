# frozen_string_literal: true

class GoogleGeocoder
  API_KEY = ENV['API_KEY'].freeze
  include HTTParty
  base_uri 'https://maps.googleapis.com'

  def geocode(address)
    options = { query: { address: address, key: API_KEY } }
    response = self.class.get('/maps/api/geocode/json', options)
    GoogleGeocoderResult.new(
      response['status'], response['results'], response['error_message']
    )
  end
end
