# Geocoder

This is a web application that receives an address as a string, and outputs the longitude and latitude of the corresponding address in JSON format.

## Design decisions

I decided to do API versioning for the endpoint because it'll never happen to change the response which it returns.

I assume that other web applications and mobile devices will call the endpoint, thus it would be better to introduce the Bearer Token authentication into the application rather than the Digest authentication. But I added the Digest authentication to this application in order to be easily satisfied with the condition that it should not be publicly accessible.

An API key of Google Cloud Platform should be injected into the application as environment variables. Because the credentials such as API keys should not be hardcoded.

I thought that it would be nice to use Elasticsearch to store addresses, latitude, and longitude since I can reduce a number of calling the Google Geocoding API. But I stopped using Elasticsearch because it'll be overengineered.


## Getting Started

### Prerequisites

Before running this application, you should get an API key from the Google Cloud Platform Console to convert addresses into a latitude and longitude via Geocoding API of the Google Maps Platform. You can get it [here](https://developers.google.com/maps/documentation/geocoding/get-api-key).

### Installing

* Install Ruby `2.5.1` and `bundler`
* Run `bundle install`
* Execute `API_KEY=YOUR_GCP_API_KEY bundle exec rackup -p 4567` to start up your local application
* Send a `GET` request to the application like the following

```
curl --digest -u 'user:geocode' -i -H 'Accept: application/json' "http://localhost:4567/api/v1/geocode/checkpoint%20charlie"
```

### Running the specs

* Execute `bundle exec rspec`
