# frozen_string_literal: true

RSpec.describe GoogleGeocoderResult do
  describe '#location' do
    let(:status) { '' }
    let(:results) { [] }
    let(:error_message) { '' }
    let(:google_geocoder_result) do
      described_class.new(status, results, error_message)
    end

    subject { google_geocoder_result.location }

    context 'when an instance is valid' do
      let(:status) { 'OK' }
      let(:results) do
        [
          { 'geometry' => {'location' => { 'lat' => 3, 'lng' => 4  } } }
        ]
      end
      let(:expected) { { location: { latitude: 3, longitude: 4 } } }

      it { is_expected.to eq(expected) }
    end

    context 'when an instance is invalid' do
      let(:status) { 'INVALID' }
      let(:expected) { { location: { latitude: '', longitude: '' } } }

      it { is_expected.to eq(expected) }
    end
  end
end
