# frozen_string_literal: true

RSpec.describe Geocoder do
  def app
    described_class
  end

  describe 'GET api/v1/geocode/:address' do
    let(:address) { 'address' }
    let(:request) { get "/api/v1/geocode/#{address}" }
    subject do
      request
      last_response
    end

    context 'when a request is authorized' do
      let(:location) { { location: { latitude: 3, longitude: 4 } } }
      let(:google_geocoder_result) { instance_double(GoogleGeocoderResult) }

      before do
        digest_authorize 'user', 'geocode'
        allow(GoogleGeocoder).to receive_message_chain(:new, :geocode).with(address).and_return(google_geocoder_result)
      end

      context 'when a response is valid' do
        before do
          allow(google_geocoder_result).to receive(:valid?).and_return(true)
          allow(google_geocoder_result).to receive(:location).and_return(location)
        end

        it { expect(subject.status).to be 200 }
      end

      context 'when a response is invalid' do
        let(:status) { 'INVALID' }
        let(:error_message) { 'Serious error' }
        let(:message) {  { message: "#{status}: #{error_message}"}  }
        before do
          allow(google_geocoder_result).to receive(:valid?).and_return(false)
          allow(google_geocoder_result).to receive(:message).and_return(message)
        end

        it { expect(subject.status).to be 422 }
        it { expect(subject.body).to eq(message.to_json)}
      end
    end

    context 'when a request is not authorized' do
      it { expect(subject.status).to be 401 }
    end
  end
end
