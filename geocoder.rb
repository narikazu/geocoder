# frozen_string_literal: true

require 'httparty'
require 'sinatra/base'
require 'sinatra/namespace'
require 'sinatra/reloader'
require 'sinatra/json'

FILE_PATH = File.dirname(__FILE__)
Dir[FILE_PATH + '/lib/*.rb'].each do |file|
  require file
end

class Geocoder < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  register Sinatra::Namespace

  namespace '/api' do
    before do
      content_type :json
    end

    namespace '/v1' do
      get '/geocode/:address' do
        google_geocoder_result = GoogleGeocoder.new.geocode(params['address'])

        if google_geocoder_result.valid?
          json google_geocoder_result.location
        else
          halt 422, google_geocoder_result.message.to_json
        end
      end
    end
  end

  def self.new(*)
    app = Rack::Auth::Digest::MD5.new(super) do |username|
      { 'user' => 'geocode' }[username]
    end
    app.realm = 'Protected Area'
    app.opaque = 'secretkey'
    app
  end
end
